package com.example.fangfanghu.fadeinmessage;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;


public class MyActivity extends Activity {

    Button buttonNext;
    TextSwitcher textSwitcher;

    String[] TextToSwitched = { "Sunday", "Monday", "Tuesday", "Wednesday",
            "Thursday", "Friday", "Saturday" };

    int curIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        buttonNext = (Button) findViewById(R.id.next);
        textSwitcher = (TextSwitcher) findViewById(R.id.textswitcher);

//        Animation fadeIn = AnimationUtils.loadAnimation(this,
//                android.R.anim.fade_in);
//        Animation fadeOut = AnimationUtils.loadAnimation(this,
//                android.R.anim.fade_out);

        AlphaAnimation fadeIn = new AlphaAnimation(0.0f , 1.0f ) ;
        AlphaAnimation fadeOut = new AlphaAnimation( 1.0f , 0.0f ) ;

        fadeIn.setDuration(3000);
        fadeIn.setFillAfter(true);

        fadeOut.setDuration(1000);
        fadeOut.setFillAfter(true);
        fadeIn.setStartOffset(1500+fadeOut.getStartOffset());

        textSwitcher.setInAnimation(fadeIn);
        textSwitcher.setOutAnimation(fadeOut);

        textSwitcher.setFactory(new ViewSwitcher.ViewFactory(){

            @Override
            public View makeView() {
                TextView textView = new TextView(MyActivity.this);
                textView.setTextSize(30);
                textView.setTextColor(Color.RED);
                textView.setGravity(Gravity.CENTER_HORIZONTAL);
                textView.setTypeface(Typeface.DEFAULT_BOLD);
                textView.setShadowLayer(10, 10, 10, Color.BLACK);
                return textView;
            }});

        curIndex = 0;


        final Runnable setTextRunnable = new Runnable() {
            public void run() {
                if(curIndex == TextToSwitched.length-1){
                    curIndex = 0;
                    textSwitcher.setText(TextToSwitched[curIndex]);
                }else{
                    textSwitcher.setText(TextToSwitched[++curIndex]);
                }
            }
        };

        TimerTask task = new TimerTask(){
            public void run() {
                runOnUiThread(setTextRunnable);
            }
        };

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(task, 0, 4000);

        buttonNext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if(curIndex == TextToSwitched.length-1){
                    curIndex = 0;
                    textSwitcher.setText(TextToSwitched[curIndex]);
                }else{
                    textSwitcher.setText(TextToSwitched[++curIndex]);
                }
            }
        });

    }

    private class DownloadFilesTask extends AsyncTask<String, Integer, Long> {
        protected Long doInBackground(String... s ) {
            Timer timer = new Timer("desired_name");
            timer.scheduleAtFixedRate(
                    new TimerTask() {
                        public void run() {
                            //switch your text using either runOnUiThread() or sending alarm and receiving it in your gui thread
                            if(curIndex == TextToSwitched.length-1){
                                curIndex = 0;
                                textSwitcher.setText(TextToSwitched[curIndex]);
                            }else{
                                textSwitcher.setText(TextToSwitched[++curIndex]);
                            }
                        }
                    }, 0, 4000);
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {

        }

        protected void onPostExecute(Long result) {

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
